## Wildfly Application Server running on OpenShift##

1. Create an OpenShift DIY application.
```
$ rhc app create groovy diy
```

2. Add git remote and pull the sourcecode
```
$ git remote add upstream -m master https://github.com/ehsavoie/groovy-openshift-quickstart.git
$ git pull -s recursive -X theirs upstream master
```

3. Git push source code
```
$ git push
```

4. Ratpack application server will be accessible at http://groovy-{domain-name}.rhcloud.com
